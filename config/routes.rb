Rails.application.routes.draw do
  resources :todos do
    resources :comments
  end

  resources :users, only: [ :index ]

  root to: 'todos#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
