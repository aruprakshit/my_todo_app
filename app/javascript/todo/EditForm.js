import React, { Component } from "react";
import axios from "axios-es6";

import TagContainer from "./Tags/TagsContainer";


const EditForm = props => {
  let title = null;

  let formAttributes = {
    titleInp: null,
    bodyInp:  null,
    authorInp: null,
  }

  const users = props.users.slice(1).map((author, index) => (
    <option key={index} value={author}>
      {author}
    </option>
  ));


  const handleSubmitForm = e => {
    e.preventDefault();

    props.handleItemUpdate(props.todo.id, {
      title:  formAttributes.titleInp.value,
      body:   formAttributes.bodyInp.value,
      author: formAttributes.authorInp.value,
    });

    props.handleHideForm();
  };

  return (
    <div className='row'>
      <div className='col bg-light'>
        <form
          className="p-2"
          onSubmit={handleSubmitForm} >
          <div className="row form-group">
            <div className='col'>
              <label htmlFor="selectAuthors">Select Author</label>
              <select
                className="form-control"
                id="selectAuthors"
                ref={input => formAttributes.authorInp = input}
                defaultValue={props.todo.author}>
                {users}
              </select>
            </div>
          </div>
          <div className="row form-group">
            <div className="col">
              <label htmlFor="todoTitle">Title</label>
              <input
                type="text"
                id="todoTitle"
                className="form-control"
                defaultValue={props.todo.title}
                ref={input => formAttributes.titleInp = input}
                placeholder="Add task title here.." />
            </div>
          </div>
          <div className="row form-group">
            <div className="col">
              <label htmlFor="todoBody">Body</label>
              <textarea
                id="todoBody"
                className="form-control"
                defaultValue={props.todo.body}
                ref={input => formAttributes.bodyInp = input}
                placeholder="Add task description here..." />
            </div>
          </div>
          <div className="row form-group">
            <div className="col-12">
              <TagContainer />
            </div>
          </div>
          <div className="row form-group">
            <div className="col-4 d-flex">
              <button type="submit" className="btn btn-sm btn-outline-primary mr-auto">
                Submit
              </button>
              <button
                type="button"
                className="btn btn-sm btn-outline-danger ml-auto"
                onClick={e => props.handleHideForm()}
              >
                Cancel
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default EditForm;
