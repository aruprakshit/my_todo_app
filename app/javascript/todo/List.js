import React, {Component} from 'react';
import PropTypes from 'prop-types';
import axios from 'axios-es6';

import ItemContainer from './ItemContainer';
import NewForm from './NewForm';
import Alert from './Alert';
import Filter from './Filter';
import Main from './Comments/Main';

class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      todos: [],
      showNewForm: false,
      showAlert: false,
      users: ['All'],
      displayComments: [],
    };

    this.handleItemUpdate = this.handleItemUpdate.bind(this);
    this.handleItemDelete = this.handleItemDelete.bind(this);
    this.addNewItemToList = this.addNewItemToList.bind(this);
    this.handleToggleNewForm = this.handleToggleNewForm.bind(this);
    this.handleDisplayAlert = this.handleDisplayAlert.bind(this);
    this.getFilteredItems = this.getFilteredItems.bind(this);
    this.handleToggleComments = this.handleToggleComments.bind(this);

    this.alertMessage = null;
    this.filterByAuthor = null;
  }

  handleToggleComments(todo) {
    if (this.state.displayComments.includes(todo.id)) {
      this.setState({displayComments: []})
    } else {
      this.setState({displayComments: [todo.id]})
    }
  }

  handleToggleNewForm(e) {
    if (e) e.preventDefault();

    this.setState({showNewForm: !this.state.showNewForm})
  }

  handleDisplayAlert(e) {
    this.setState(prev => ({
      showAlert: !prev.showAlert
    }));
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextState.todos.length === this.state.todos.length) {
      this.alertMessage = 'Item is updated successfully.'
    } else if (nextState.todos.length > this.state.todos.length) {
      this.alertMessage = 'Item is created successfully.'
    } else {
      this.alertMessage = 'Item is deleted successfully.'
    }
  }

  getFilteredItems(searchToken) {
    axios
    .get(`/todos.json?q=${searchToken}`)
    .then(res => {
      const todos = res.data.map(obj => obj);
      this.filterByAuthor = searchToken;
      this.setState({ todos });
    });
  }

  componentDidMount() {
    axios
      .get('/todos.json')
      .then(res => {
        const todos = res.data.map(obj => obj);
        this.setState({ todos });
      });

    axios
      .get('/users.json')
      .then(res => {
        const users = res.data.map(obj => obj);

        this.setState(prev => ({
          users: [...prev.users, ...users]
        }));
      });
  }

  handleItemUpdate(id, params) {
    let newObject = null;

    axios
      .put(
        `/todos/${id}.json`,
        params,
        { headers: { "X-CSRF-Token": this.props.authToken } }
      )
      .then(response => {
        newObject = response.data;
        const itemIndex = this.state.todos.findIndex(record => record.id === id);
        const copyTodos = [...this.state.todos];
        copyTodos.splice(itemIndex, 1, newObject);

        this.setState({
          todos: copyTodos,
          showAlert: true
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleItemDelete(id) {
    const itemIndex = this.state.todos.findIndex(record => record.id === id);
    const copyTodos = [...this.state.todos];

    copyTodos.splice(itemIndex, 1);

    this.setState({
      todos: copyTodos,
      showAlert: true
    });
  }

  addNewItemToList(params) {
    let todos = this.state.todos.slice;

    axios
      .post(
        '/todos.json',
        {
          todo: params
        },
        { headers: { "X-CSRF-Token": this.props.authToken } }
      )
      .then(response => {
        this.handleToggleNewForm();
        this.setState(prev => (
          {todos: [...prev.todos, response.data], showAlert: true}
        ))
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    const list = this.state.todos.map(todo => (
      <ItemContainer
        {...this.props}
        users={this.state.users}
        key={todo.id}
        handleItemDelete={this.handleItemDelete}
        handleItemUpdate={this.handleItemUpdate}
        handleToggleComments={this.handleToggleComments}
        displayComments={this.state.displayComments}
        todo={todo} />
    ));

    const headerText = this.filterByAuthor ?
      `List for ${this.filterByAuthor}` :
      'All Items';

    return (
      <div className="container p-5">
        <h1 className='text-center bg-white p-1 border border-light text-capitalize font-weight-bold'>{headerText}</h1>
        <div className='row'>
          <div className='col-3'>
            <Filter getFilteredItems={this.getFilteredItems} users={this.state.users}/>
          </div>
          <div className='col-6'>
            <div className='row'>
              <div className='col'>
                <Alert
                  handleDisplayAlert={this.handleDisplayAlert}
                  showAlert={this.state.showAlert}>
                  {this.alertMessage}
                </Alert>
              </div>
            </div>
            {!this.state.showNewForm &&
              <div className='row'>
                <div className='col'>
                  <a
                    role="button"
                    href="#"
                    onClick={ e => this.handleToggleNewForm(e) }
                    className="btn btn-outline-primary btn-lg btn-block mb-2">Add New</a>
                </div>
              </div>
            }
            <div className='row'>
              <div className='col'>
                <NewForm
                  handleNewItem={this.addNewItemToList}
                  handleToggleNewForm={this.handleToggleNewForm}
                  users={this.state.users}
                  showForm={this.state.showNewForm}/>
              </div>
            </div>
            {list}
          </div>
          <div className='col-3'>
            { this.state.displayComments.length !== 0 &&
              <Main
                authToken={this.props.authToken}
                todoId={this.state.displayComments[0]}/>
            }
          </div>
        </div>
      </div>
    );
  }
}

List.propTypes = {
  name: PropTypes.string,
}

List.defaultProps = {
  name: 'Arup',
};

export default List;
