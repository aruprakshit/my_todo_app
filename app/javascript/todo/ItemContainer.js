import React, { Component } from "react";
import EditForm from './EditForm';
import Item from './Item';


class ItemContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      edit: false,
    };
  }

  handleShowForm() {
    this.setState({edit: true});
  }

  handleHideForm() {
    this.setState({edit: false});
  }

  render() {
    const itemOrForm = this.state.edit ?
      <EditForm
        {...this.props}
        todo={this.props.todo}
        handleHideForm={() => this.handleHideForm()} /> :
      <Item
        {...this.props}
        todo={this.props.todo}
        handleShowForm={() => this.handleShowForm()} />
    return <div> { itemOrForm } </div>
  }
}

export default ItemContainer;
