import React, { Component } from "react";
import TagContainer from "./Tags/TagsContainer";

const NewForm = props => {
  if (!props.showForm) return null;

  let formAttributes = {
    titleInp: null,
    bodyInp: null,
    authorInp: null
  };

  const formSubmitHandler = e => {
    e.preventDefault();

    props.handleNewItem({
      title: formAttributes.titleInp.value,
      body: formAttributes.bodyInp.value,
      author: formAttributes.authorInp.value
    });
  };

  const handleFormHide = e => {
    e.preventDefault();
    props.handleToggleNewForm();
  };

  const users = props.users.slice(1).map((author, index) => (
    <option key={index} value={author}>
      {author}
    </option>
  ));

  return (
    <div className="card bg-light border-light">
      <div className="card-body">
        <h4 className="lead card-title"> Create a new item </h4>
        <form onSubmit={formSubmitHandler}>
          <div className="row form-group">
            <div className="col">
              <label htmlFor="selectAuthors">Select Author</label>
              <select
                className="form-control"
                id="selectAuthors"
                ref={input => (formAttributes.authorInp = input)}
                defaultValue=""
              >
                {users}
              </select>
            </div>
          </div>
          <div className="row form-group">
            <div className="col">
              <input
                type="text"
                className="form-control"
                defaultValue=""
                ref={input => (formAttributes.titleInp = input)}
                placeholder="Add task title"
              />
            </div>
          </div>
          <div className="row form-group">
            <div className="col">
              <input
                type="text"
                className="form-control"
                defaultValue=""
                ref={input => (formAttributes.bodyInp = input)}
                placeholder="Add task description here..."
              />
            </div>
          </div>
          <div className="row form-group">
            <div className="col-12">
              <TagContainer />
            </div>
          </div>
          <input
            type="submit"
            className="btn btn-outline-primary mr-4"
            value="Save"
          />
          <button
            type="button"
            className="btn btn-outline-danger"
            onClick={handleFormHide}
          >
            Discard
          </button>
        </form>
      </div>
    </div>
  );
};

export default NewForm;
