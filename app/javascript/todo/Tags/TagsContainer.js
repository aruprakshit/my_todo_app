import React, { Component } from "react";
import TagItem from "./TagItem";

class TagsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tags: [
        "Ruby",
        "JavaScript",
        "Elixir",
        "React",
        "Angular",
        "Rails",
        "EmberJs",
        "Scala",
        "Python"
      ]
    };
  }

  get styles() {
    return {
      margin: "2px",
      padding: "2px 4px 2px",
      fontSize: '12px',
    };
  }

  get colors() {
    return function* colorGenerator() {
      yield "#cc342d"; // Ruby
      yield "#f1e05a"; // JavaScript
      yield "#6e4a7e"; // Elixir
      yield "#61dafb"; // React
      yield "#e34c26"; // Angular
      yield "#cc0000"; // Rails
      yield "#e47051"; // EmberJs
      yield "#a4302e"; // Scala
      yield "#2b5b84"; // Python
    };
  }

  renderTagItems() {
    const colorIterator = this.colors();

    return this.state.tags.map((tagName, index) => (
      <TagItem
        tagName={tagName}
        styles={{ backgroundColor: colorIterator.next().value, ...this.styles }}
        key={index}
      />
    ));
  }

  render() {
    return <div className="d-flex flex-wrap"> {this.renderTagItems()} </div>;
  }
}

export default TagsContainer;
