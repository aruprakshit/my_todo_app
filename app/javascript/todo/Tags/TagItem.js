import React from 'react';

const TagItem = ({tagName, styles}) =>
  <span className='rounded text-white outerShadow' style={styles}> {tagName} </span>

export default TagItem;
