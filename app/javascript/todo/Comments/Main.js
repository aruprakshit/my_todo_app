import React, { Component } from "react";
import axios from 'axios-es6';

import NewForm from './NewForm'

export default class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      comments: []
    }

    this.addNewComment = this.addNewComment.bind(this);
  }

  addNewComment(params) {
    let comments = this.state.comments.slice;

    axios
      .post(
        `/todos/${this.props.todoId}/comments.json`,
        {
          comment: params
        },
        { headers: { "X-CSRF-Token": this.props.authToken } }
      )
      .then(response => {
        this.setState(prev => (
          {comments: [...prev.comments, response.data]}
        ))
      })
      .catch(error => {
        console.log(error);
      });
  }

  componentDidMount() {
    axios
      .get(`/todos/${this.props.todoId}/comments.json`)
      .then(res => {
        const comments = res.data.map(obj => obj);
        this.setState({ comments });
      });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.todoId !== this.props.todoId) {
      axios
        .get(`/todos/${nextProps.todoId}/comments.json`)
        .then(res => {
          const comments = res.data.map(obj => obj);
          this.setState({ comments });
        });
    }
  }

  render() {
    const comments = this.state.comments.map(comment =>(
      <li className="list-group-item" key={comment.id}>{comment.description}</li>
    ));

    return (
      <div className="card border border-light">
        <div className="card-body">
          <h4 className="card-title">Comments</h4>
          <NewForm addNewComment={this.addNewComment}/>
        </div>
        <ul className="list-group list-group-flush p-1">
          {comments}
          { comments.length === 0 &&
            <li className="list-group-item" >No Comments</li>
          }
        </ul>
      </div>
    );
  }
}
