import React, { Component } from "react";

const NewForm = props => {
  let desciptionInp;

  const handleSubmit = e => {
    e.preventDefault();
    props.addNewComment({description: desciptionInp.value});
    e.currentTarget.reset();
  }

  return (
    <form className="card-text" onSubmit={handleSubmit}>
      <div className="form-group">
        <textarea
          className="form-control"
          placeholder="Add desciption..."
          defaultValue={props.desciption}
          ref={input => desciptionInp = input}
        />
      </div>
      <button type="submit" className="btn btn-sm btn-primary">
        Save
      </button>
    </form>
  );
};

export default NewForm;
