import React from 'react';
import ReactDOM from 'react-dom';
import List from './List'

import './styles/index'

document.addEventListener('DOMContentLoaded', () => {
  const authToken = document.querySelectorAll('meta[name="csrf-token"]')[0].content;

  ReactDOM.render(
    <List authToken={authToken}/>,
    document.getElementById('todoMain'),
  );
})
