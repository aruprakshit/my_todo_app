import React, {Component} from 'react';
import axios from 'axios-es6';

class Item extends Component {
  handleDeleteItem(e) {
    e.preventDefault();

    axios
      .delete(`/todos/${props.todo.id}.json`,{
        headers: {'X-CSRF-Token': props.authToken }
      })
      .then(response => {
        this.props.handleItemDelete(props.todo.id);
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleToggleComments(e) {
    e.preventDefault();
    this.props.handleToggleComments(this.props.todo);
  }

  render() {
    const isCommentsOpen = this.props.displayComments.includes(this.props.todo.id);

    return (
      <div className="row rounded Item mt-1 mb-1 p-1">
        <div className="col-6 text-white align-self-center">{this.props.todo.title}</div>
        <div className="col-6 d-flex">
          <button
            type="button"
            className="btn btn-outline-primary btn-sm ml-auto"
            onClick={e => this.props.handleShowForm()}>Edit</button>

          <button
            type="button"
            className={`btn btn-outline-info btn-sm ml-auto ${isCommentsOpen && 'active'}`}
            onClick={ (e) => this.handleToggleComments(e) }>Comments</button>

          <button
            type="button"
            className="btn btn-outline-danger btn-sm ml-auto"
            onClick={this.handleDeleteItem}>Delete</button>
        </div>
      </div>
    );
  }
}

export default Item;
