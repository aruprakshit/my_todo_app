import React from 'react';

const Alert = (props) => {
  if (!props.showAlert) return null;

  const handleCloseBtn = e => {
    e.preventDefault();
    props.handleDisplayAlert();
  }

  return (
    <div className="alert alert-success" role="alert">
      <button type="button" className="close" aria-label="Close" onClick={handleCloseBtn}>
          <span aria-hidden="true">&times;</span>
      </button>
      {props.children}
    </div>
  )
}

export default Alert;
