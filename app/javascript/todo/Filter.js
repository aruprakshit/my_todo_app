import React, { Component } from "react";
import axios from 'axios-es6';

class Filter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      openMenu: false,
      selectedUser: null,
    }

    this.styles = {
      'whiteSpace': 'normal'
    }
  }

  handleMenuDisplay(e) {
    e.preventDefault();

    this.setState(prev => ({openMenu: !prev.openMenu}));
  }

  handleCiickOnUser(e) {
    e.preventDefault();

    if (e.currentTarget.textContent === 'All') {
      this.setState({selectedUser: null, openMenu: false});
      this.props.getFilteredItems('');
    } else {
      this.setState({selectedUser: e.currentTarget.textContent, openMenu: false});
      this.props.getFilteredItems(e.currentTarget.textContent);
    }
  }

  render () {
    let dropdownItems;

    if (this.state.selectedUser) {
      dropdownItems = this.props.users.map((userName, index) => (
        <a key={index} className="dropdown-item" href="#" onClick={e => this.handleCiickOnUser(e)}>{userName}</a>
      ));
    } else {
      dropdownItems = this.props.users.slice(1).map((userName, index) => (
        <a key={index} className="dropdown-item" href="#" onClick={e => this.handleCiickOnUser(e)}>{userName}</a>
      ));
    }

    return (
      <div className="card bg-light">
        <div className="card-body">
          <h5 className="card-title lead">Select items by users: </h5>
          <div className={`dropdown ${this.state.openMenu ? 'show' : ''}`}>
            <button
              className="btn btn-secondary dropdown-toggle w-100"
              style={this.styles}
              type="button" id="filterByUsers"
              data-toggle="dropdown"
              aria-haspopup="true"
              onClick={e => this.handleMenuDisplay(e)}
              aria-expanded={this.state.openMenu}>
              {this.state.selectedUser || 'Select an user'}
            </button>
            <div className={`dropdown-menu ${this.state.openMenu ? 'show' : ''}`} aria-labelledby="filterByUsers">
              {dropdownItems}
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default Filter;
