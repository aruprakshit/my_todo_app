class UsersController < ApplicationController
  def index
    @users = User::NAMES

    respond_to do |format|
      format.json { render json: @users }
    end
  end
end
