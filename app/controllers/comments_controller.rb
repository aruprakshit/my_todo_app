class CommentsController < ApplicationController
  before_action :set_todo

  def index
    @comments = @todo.comments

    respond_to do |format|
      format.json { render json: @comments }
    end
  end

  def create
    @comment = Comment.new(comment_params.merge(todo_id: @todo.id))

    respond_to do |format|
      if @comment.save
        format.json { render json: @comment }
      else
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_todo
    @todo = Todo.find(params[:todo_id])
  end

  def comment_params
    params.require(:comment).permit(:description)
  end
end
