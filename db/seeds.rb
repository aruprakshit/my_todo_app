# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Todo.create!(
  [
    {
      title: 'Get Fish',
      body: 'Auctor est scelerisque montes arcu a condimentum netus consectetur massa nisl malesuada condimentum dictum nisi diam quam sed.',
      finished: false,
      author: 'Gracie Weber'
    },
    {
      title: 'Cook Fish',
      body: 'Auctor malesuada cum vestibulum vivamus pulvinar vel vehicula vestibulum malesuada potenti id per ac pharetra condimentum platea iaculis lacinia per blandit augue.',
      finished: false,
      author: 'Gracie Weber'
    },
    {
      title: 'Serve Fish',
      body: 'Vestibulum adipiscing condimentum sodales sociis inceptos at parturient erat non erat a mi commodo metus.',
      finished: false,
      author: 'Keegan Thiel'
    },
    {
      title: 'Shut the Door',
      body: 'Sed enim a augue vestibulum eget aliquet lorem amet parturient etiam nisi dignissim odio tortor cursus et ridiculus quisque lectus lorem nulla a.',
      finished: false,
      author: 'Emmett Lebsack'
    }
  ]
)
