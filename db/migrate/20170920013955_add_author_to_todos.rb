class AddAuthorToTodos < ActiveRecord::Migration[5.1]
  def change
    add_column :todos, :author, :string
  end
end
