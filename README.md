## README

- https://devcenter.heroku.com/articles/getting-started-with-rails5

# How to handle ActionController::InvalidAuthenticityToken error?

Add `X-CSRF-Token` to header for `DELETE`, `POST`, `PUT` request. Or, `POST`, `PUT` etc add `authenticity_token` to the body of the request. This is documented:

- https://github.com/rails/rails/blob/896ee2b679462e7bcaa6890c6f5fdbd3b2e2fccb/actionview/app/assets/javascripts/rails-ujs/utils/csrf.coffee#L18
- https://github.com/rails/rails/blob/7a9b93805bd6f901de72c8f84c75e4f52476a872/actionpack/lib/action_controller/metal/request_forgery_protection.rb
- https://github.com/rails/rails/blob/46c2e93051ab33157c584292a2743f9482e99582/actionview/lib/action_view/helpers/csrf_helper.rb#L19
- https://github.com/rails/rails/blob/34956f7422798f27f8926544d58771042f6f1c3a/guides/source/security.md#L259

# Online editor to share React code
- https://www.codesandbox.io/

# Add Generator through babel-polyfill
- https://github.com/rails/webpacker/issues/523
